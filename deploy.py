#! /usr/bin/env python
# -*- coding: utf-8 -*-

"""
This is a script for packaging roadNet and deploying it to an AWS repository.

It is run by the continuous integration system following a successful build.

Files are copied to _repository_files_ directory.
"""

import datetime as dt
import os
import sys
import subprocess
from textwrap import dedent

ZIPFILE_NAME = 'qGeoDataHub.zip'
OUTPUT_FOLDER = 'public'
BASE_URL = 'http://plugins.geodatahub.dk/qgis/qGeoDataHub'
version = '1.0.2'


def main(move_zip=False):
    # Update files with versions and dates
    try:
        os.makedirs(f"{OUTPUT_FOLDER}/qGeoDataHub")
    except FileExistsError:
        pass
    branch, build_name, version, xml_filename, zip_path = get_build_and_version()
    if move_zip:
        if branch is not None and branch != "master":
            try:
                plugin_dir = f"{OUTPUT_FOLDER}/qGeoDataHub/{branch}"
                print(f"Creating {plugin_dir}")
                os.makedirs(plugin_dir)
                os.rename('zip_build/qGeoDataHub.zip', f"public/qGeoDataHub/{branch}/qGeoDataHub-{version}.zip")
            except FileExistsError:
                pass
        else:
            os.rename('zip_build/qGeoDataHub.zip', f"public/qGeoDataHub/qGeoDataHub-{version}.zip")
    else:
        update_metadata(version)
        write_plugins_xml(version, xml_filename, zip_path)


def get_build_and_version():
    """
    Return timestamp in seconds since epoch as int for version number
    :return:
    """
    branch = os.getenv('CI_COMMIT_BRANCH', None)
    tag = os.getenv('CI_COMMIT_GITTAG', None)
    commit = os.getenv('CI_COMMIT_SHA', 'aaaaaaaa')[:8]

    # Show environmental settings to user
    print(f"Branch name: {branch}")
    print(f"Tag name: {tag}")
    print(f"Commit sha: {commit}")

    # Get number of commits on the current branch
    build_name = '{}.{}'.format(branch, commit)
    if branch and branch != "master":
        print(f"Building a release branch named {build_name}")
        try:
            build_num = os.environ["GDH_BRANCH_COMMITS"]
        except KeyError:
            build_num = subprocess.check_output(['git', 'rev-list', '--count', 'HEAD', '^master'],
                                                shell=False,
                                                text=True)

        version = '{}.{}'.format(branch, build_num.strip())
        filename = f"plugins-{branch}.xml"
        zip_path = f"{BASE_URL}/{branch}/qGeoDataHub-{version}.zip"
    elif tag:
        print(f"Building a tagged release named {tag} on branch {build_name}")
        version = tag
        filename = f"plugins-releases.xml"
        zip_path = f"{BASE_URL}/qGeoDataHub-{version}.zip"
    else:
        raise RuntimeError("You cannot deploy directly from the master branch. Please set the correct release tag and try again.")

    return branch, build_name, version, filename, zip_path


def update_metadata(version):
    """
    Rewrites metadata.txt with the new version number replaced.
    :param version: string with branch and build
    """
    new_version_line = 'version={}'.format(version)
    replace_line_in_file('metadata.txt', 'version=', new_version_line)


def replace_line_in_file(filename, line_start, replacement):
    """
    Rewrite a file, replacing a specific line with another.
    :param filename: file to update
    :param line_start: opening characters of line to replace
    :param replacement: string of replacement line
    """
    with open(filename, 'r') as f:
        original_text = f.readlines()
    with open(filename, 'w') as f:
        for line in original_text:
            if line.startswith(line_start):
                line = replacement + '\n'
            f.write(line)


def write_plugins_xml(version, filename, zip_path):
    """"
    Write a plugins.xml file with new version number.
    :build_name: string with branch and build
    :param version: string with branch and build and commit
    :param build_name: string with branch and build
    """
    now = dt.datetime.now().strftime('%Y-%m-%d')
    xml_string = """\
        <?xml version = '1.0' encoding = 'UTF-8'?>
        <?xml-stylesheet type='text/xsl' href='/plugins.xsl' ?>
        <plugins>
          <pyqgis_plugin name='qGeoDataHub' version='{version}'>
            <description>Online repository for geosciencetific data.</description>
            <version>{version}</version>
            <qgis_minimum_version>3.0</qgis_minimum_version>
            <homepage>https://www.geodatahub.dk</homepage>
            <file_name>qGeoDataHub.zip</file_name>
            <author_name>NextGen Geophysics</author_name>
            <download_url>{zip_path}</download_url>
            <uploaded_by>NextGen Geophysics</uploaded_by>
            <create_date>2020-04-20</create_date>
            <update_date>{update_date}</update_date>
          </pyqgis_plugin>
        </plugins>
    """.format(version=version, update_date=now, zip_path=zip_path)
    xml_string = dedent(xml_string)

    with open(f"{OUTPUT_FOLDER}/{filename}", 'wt') as f:
        f.write(xml_string)


def clean_up():
    os.remove('plugins.xml')


if __name__ == '__main__':
    try:
        clean_up()
    except Exception:
        pass

    move_zip = False
    if len(sys.argv) > 1:
        # Move the zip file if this script is called with any number of
        # arguments
        print("Moving zip file")
        move_zip = True
    main(move_zip)
