"""Log python errors to QGIS

This script implements a new handler for
the python logging module that sends messages
to QGIS as logMessages.
"""
from logging import StreamHandler
from qgis.core import QgsMessageLog, Qgis


class QGISHandler(StreamHandler):
    def __init__(self, plugin_name):
        StreamHandler.__init__(self)
        self.name = plugin_name
        self._iface = None

    def emit(self, record):
        """Emit the log message to stream

        Paramters
        ---------
        record: LogRecord
            The log object to emit

        Also see
        --------
        https://docs.python.org/3/library/logging.html#logrecord-attributes
        """
        if self._iface and record.levelno > 30:
            # Emit Python Error (30) and Critical (50) as QGIS Critical
            # in a message bar
            self._iface.messageBar().pushMessage(record.levelname,
                                                 str(record.msg),
                                                 level=Qgis.Critical)
        elif self._iface and record.levelno == 30:
            # Emit Python Warning (30) as QGIS Warning in a message bar
            self._iface.messageBar().pushMessage(record.levelname,
                                                 str(record.msg),
                                                 level=Qgis.Warning)
        else:
            # Emit Debug and Info only in the LogMessages window
            QgsMessageLog.logMessage(str(record.msg),
                                     self.name,
                                     level=Qgis.Info)

    def attach_iface(self, iface):
        """Add a iface to the logger

        Parameters
        ----------
        iface: QGIS interface object
            Interface used to generate messageBar objects
        """
        self._iface = iface
