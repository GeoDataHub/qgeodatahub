"""Custom Qt classes to handle auto-complete

Qt has a built-in auto-complete class called QCompleter. The class is however not easy to use
in highly customized environments. In this module the standard LineEdit object is modified with
a QCompleter object but all the completion logic is moved to custom methods directly on the LineEdit.
This allows for highly customized completion results and the option to use a different matching
algorithm than Qt supports (Fuzzyfinder).
The QCompleter is only used to handle the drop-down and spoofed the completion results from the
fuzzefinder algorithm
"""

import re
import types
import json
import math
import logging as lg

from PyQt5 import QtNetwork, QtWidgets, QtCore, QtGui

import gdh_utils
LOG = lg.getLogger("geodatahub")

# Setup logging module
lg.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=lg.INFO)
qloghandler = gdh_utils.QGISHandler("GeoDataHub plugin")
qloghandler.setLevel(lg.INFO)
LOG.addHandler(qloghandler)
LOG.setLevel(lg.INFO)


def fuzzyfinder(user_input, collection):
    """Compare user input with possible options

    Vim/Emacs style fuzzy complete. This algorithm ignores white spaces and finds the
    match that shared most characters in the correct order with the values in collection.
    In this way the string `bttlshp` will match the option `battleship` but not `battery`.

    Ref: https://blog.amjith.com/fuzzyfinder-in-10-lines-of-python

    Parameters
    ----------
    user_input: str
        Text used for the match
    collection: list
        Possible completion options
    """
    suggestions = []
    pattern = '.*?'.join(user_input)   # Converts 'djm' to 'd.*?j.*?m'
    regex = re.compile(pattern, re.IGNORECASE)  # Compiles a regex.
    for item in collection:
        match = regex.search(item)   # Checks if the current item matches the regex.
        if match:
            suggestions.append((len(match.group()), match.start(), item))
    return [x for _, _, x in sorted(suggestions)]


def add_autocomplete(lineedit, data = None, api_reference = None):
    """Add autocomplete capability to an existing line edit

    This function takes an

    Parameters
    -----------
    lineedit: QLineEdit Object
        The LineEdit to add autocomplete to
    data: dict or None
        The autocomplete options or None to get options from API
    api_reference: Instance of Geodatahub API
        Link to the Geodatahub API

    Returns
    --------
    LineEdit: QLineEdit Object
        The LineEdit to add autocomplete to
    autocompleter: Instance of the APISearchCompleter class
        The auto completer attached to the linestring object
    """
    asc  = APISearchCompleter(data, api_reference)
    asc.setCaseSensitivity(QtCore.Qt.CaseInsensitive)
    asc.setWidget(lineedit)

    # Add the method to an existing lineedit object
    lineedit.update_with_completion = types.MethodType( update_with_completion, lineedit)

    # Set custom styling of the items in the popup
    # with completion selections
    lv = QtWidgets.QListView()
    lv.setItemDelegate(APIStyledItemDelegate())
    asc.setPopup(lv)

    # Update model when text has changed
    lineedit.textChanged.connect(asc.text_changed)
    # Update the search text area when the user has selected a popup
    asc.activated.connect(lineedit.update_with_completion)

    return lineedit, asc


def text_to_parameters(text, only_last_match=True):
    """Extract current parameters from a search line string

    Examples
    --------
    Given the text,

        "orgs/borehole/myschema.borehole depth"=12

    This function returns,

        schema   : orgs/borehole/myschema
        parameter: depth
        delimiter: =
        value    : 12

    Note that the schema and depth parameters is stripped of the quotation.

    If some parameters do not exist then they are None,

        orgs/borehole/myschema.depth

    This function returns,

        schema   : orgs/borehole/myschema
        parameter: depth
        delimiter: None
        value    : None

    Parameters
    -----------
    text: str
        String to extract parameters from
    only_last_match: bool
        Only return the last match in the string

    Returns
    -------
    schema: text or None
        The extracted schema at the cursor location (e.g. https://schemas.geodatahub.dk/myschema)
    parameter: text or None
        The extracted parameters at the cursor location (e.g. Site Name)
    delimiter: str or None
        The extracted delimiter at the cursor location (e.g. =)
    value: str or None
        The extracted value at the cursor location (e.g. Copenhagen Site 1)

    Notes
    -----
    All strings are striped of special characters: \"
    """
    SEARCH_FORMAT_REGEX = r"""\"?\s?                      # Ignore spaces and quotation mark prior to a schema group
                              (?P<schema>[^\.]+)?         # The schema group contains any characters except a punctuation
                              \.                          # Period is the delimiter between a schema and the parameter
                              (?P<parameter>[^=><\"]+)?   # The parameter group contains any character except the logical operators
                              \s*\"?\s*                   # Ignore spaces and quotation mark prior to a schema group
                              (?P<operator>=|<|>)?        # The operator group
                              \s*                         # Ignore spaces after the operator
                              (?P<value>\".*\"|\S+)?      # The value group is either all characters between quotation mark
                                                          # or everything until the first space
    """
    match = re.findall(SEARCH_FORMAT_REGEX,
                       text,
                       re.IGNORECASE | re.VERBOSE)

    if len(match) < 1:
        return None, None, None, None

    try:
        all_matches = []
        for m in match:
            # Clean up matched groups
            clean_params = [param.strip('"').strip() for param in m if param]
            if len(clean_params) == 4:
                schema, parameter, operator, value = clean_params
            elif len(clean_params) == 3:
                schema, parameter, operator, value = *clean_params, None
            elif len(clean_params) == 2:
                schema, parameter, operator, value = *clean_params, None, None
            elif len(clean_params) == 1:
                schema, parameter, operator, value = *clean_params, None, None, None
            all_matches.append((schema, parameter, operator, value))

        if only_last_match:
            schema, parameter, operator, value = all_matches[-1]
            return schema, parameter, operator, value
        else:
            return all_matches

    except Exception as err:
        LOG.error("The search line syntax is not valid. Check GeoDataHub log for debug information.")
        LOG.debug(f"Extracting search parameters from text \"{text}\" failed with message {str(err)}")


def guess_type_from_str(val, force_to_int=False):
    """Convert string to native type if possible

    Returns a string unless a type conversion to float or int is possible
    """
    out = None
    try:
        out = float(val)
        frac, _ = math.modf(out)
        if force_to_int and frac == 0:
            # No fraction - this is an int
            out = int(out)
    except ValueError:
        # This was a string
        out = val

    return out


def update_with_completion(self, text):
    """Update the text area with completed text

    Once the user has selected a completion option it must be added to the search area. This
    function accepts the current completion option and appends the completion option
    in the correct way.

    Parameters
    ----------
    text: str
        The auto-complete option selected by the user
    """
    previous_text    = ""
    existing_text    = " ".join(self.text().split(" ")[:-1]) + " "  # Get everything the current completion section
    completion_text  = self.text().split(" ")[-1]
    match = re.search(r"(?P<previous>.+)(?P<delimiter>\.|=|>|<|\s)(?P<current>.*)",
                      completion_text,
                      re.IGNORECASE)
    if match:
        delimiter     = match.group("delimiter")  # Current delimiter
        previous      = match.group("previous")   # Everything before current delimiter
        if delimiter == '.':
            # The user has typed `schema.parameter` and the completion is `schema.parameter 1`
            # in this case any existing text is removed and `"schema.parameter 1" is inserted
            previous_text = ""
        else:
            previous_text = previous + delimiter

        # Wrap selected completion strings in quotes
        # if performing completion on level2 (i.e. the value of a query)
        if len(text.split(" ")) > 1:
            text = f"\"{text}\""

    # Return any existing text along with the users selection
    resulting_text = (existing_text + previous_text + text).strip()
    self.setText(resulting_text)


class APISchemas(QtCore.QObject):
    """Common class to store data schemas

    This class retrieves data schemas from the geodatahub schema
    repository and stores them locally for all GUI elements that
    needs them.

    The custom signal `schema_ready` is emitted every time the schema information
    changes.
    """
    schema_ready = QtCore.pyqtSignal(dict)

    def __init__(self):
        """Create a new instance of the class"""
        super(APISchemas, self).__init__()
        self.url = "https://schemas.geodatahub.dk/full_schemas.json"
        self._data = None
        self.request_schema()

    def request_schema(self):
        """Request a fresh copy of all schemas"""
        req = QtNetwork.QNetworkRequest(QtCore.QUrl(self.url))

        self.nam = QtNetwork.QNetworkAccessManager()
        self.nam.finished.connect(self.handle_response)
        self.nam.get(req)

    def handle_response(self, reply):
        """Process the response from the schema repository

        This function is called once the request for schema information
        returns. The entire request from the schema repository is emitted
        in the `schema_ready` signal to all classes who needs it.

        Parameters
        ----------
        reply: Qt reply object
            Future that returns the response from the API
        """
        er = reply.error()

        if er == QtNetwork.QNetworkReply.NoError:
            self._data = json.loads(reply.readAll().data())
            self._data_only_properties = {}
            for key in self._data:
                self._data_only_properties[key] = self._data[key]['properties']
            LOG.info(f'Received the following data from API: {self._data}')
            self.schema_ready.emit(self._data)
        else:
            LOG.error("Error occured: ", er)
            LOG.error(reply.errorString(self._data))

    def get_data(self):
        """Get the latest schema information

        If no schema information exists the call is relayed to the
        repository and new schema information is returned. In this case
        the function will initially return an empty dict ({}) and once
        the request is completed this function returns the latest data.

        Returns
        -------
        schema (dict):
            Latest schema information

        Notes
        ------
        If the class contains schema information the `schema_ready` signal is
        called on every request.
        """
        if self._data:
            self.schema_ready.emit(self._data)
            return self._data, self._data_only_properties
        else:
            self.request_schema()
            return {}, {}


class APISearchCompleter(QtWidgets.QCompleter):

    def __init__(self, data = None, api_reference = None, *args):
        """Create a new completion object

        Parameters
        -----------
        data: dict or None
            If None is given the options are fetched from the API.
        api_reference: qGeodatahub object
            Reference to the QGeodatahub instance
        """
        QtWidgets.QCompleter.__init__(self, *args)

        self.default_model = []
        self._api_reference = api_reference or None

        if data:
            try:
                data.schema_ready.connect(self.set_model)
                _, self._data = data.get_data()
            except AttributeError:
                self._data = data
                self.set_model(data)
        else:
            # The user supplied no data so let's get it from the schemes database
            schema = APISchemas()
            schema.schema_ready.connect(self.set_model)
            self._data = {}

    @QtCore.pyqtSlot(dict)
    def set_model(self, data):
        """Set the current model

        Parameters
        -----------
        data: dict
            Nested schema of the key/values pairs to allow as search

        Notes
        ---------
        The following input data,

        {"schema": {
            "key1":[],
            "key2":[]},
        "schema2": {
            "key1": []
        }}

        Will become a model with search options,

        ["schema.key1", "schema.key2", "schema2.key1"]

        """
        self._data = {}
        for key in data:
            self._data[key] = data[key]['properties']
        self.default_model = [".".join([k, k2]) for k in data.keys() for k2 in self._data[k].keys()]
        self.setModel(QtCore.QStringListModel(self.default_model))
        self.popup().setItemDelegate(APIStyledItemDelegate(self._data))

    def get_model_from_API(self, reply):
        """Set the data model based on schemas from the API

        Query the https://schemas.geodatahub.dk API for all schemas with keys

        Parameters
        ----------
        reply: QT reply object
            Future that returns the response from the API
        """
        er = reply.error()

        if er == QtNetwork.QNetworkReply.NoError:
            self._data = json.loads(reply.readAll().data())
            LOG.info(f'Received the following data from API: {self._data}')
            self.set_model(self._data)
        else:
            print("Error occured: ", er)
            print(reply.errorString())

    def update_options(self, completion_options, completion_prefix):
        """Update the completion model

        Based on what the user has currently typed in the search area
        this function overwrites the current completion options with results
        from the text_changed function that uses fuzzyfinder.

        This function calls the complete method directly causing the
        drop-down with options to appear.

        Parameters
        -----------
        completion_options: list
            Current options that should be presented to the user
        completion_prefix: str
            Text the auto complete algorithm compared with completion options for a match

        Notes
        ------
        Since the auto complete option is moved to fuzzyfinder the build in QCompleter
        logic is completely ignored. The prefix is therefore always set to an empty string
        forcing the QCompleter logic to return all options. This means that all options
        set in the `completion_options` are presented to the user.
        """
        model = QtCore.QStringListModel(completion_options, self)
        self.setModel(model)
        self.popup().setItemDelegate(APIStyledItemDelegate(self._data))

        self.setCompletionPrefix(completion_prefix)
        self.complete()

    def text_changed(self, text):
        """Change the current LineEdit text to fit with completions

        The text currently typed in the input area might contain previously completed
        information. This function cleans the input area to only complete the latest
        expression.

        Parameters
        -----------
            text: str
                Current text in the input field
        """
        schema, parameter, operator, value = text_to_parameters(text)
        user_is_typing_new_query = False
        query_string = ""

        if value:
            # Find position of the value match
            position_of_value = text.find(value) + len(value) if text.find(value) > 0 else -1
            len_from_value_to_line_end = len(text) - position_of_value

            if position_of_value > 0 and len_from_value_to_line_end > 1:
                # User has typed something that is not yet a full match since
                # the text area is longer then the latest value match
                # E.g: my_schema.parameterN=valueN   my_new_schema
                user_is_typing_new_query = True
                query_string = text[-len_from_value_to_line_end:].strip('"').strip()

        if operator and not user_is_typing_new_query:
            # The user is currently trying to complete a specific schema values
            # E.g. The text is,
            #
            #    my_schema.parameterN=
            #
            # In that case update the current value with the specific completion options
            try:
                # Get completion options from the data that is already available
                current_options = self._data[schema][parameter]['completion_options']
            except KeyError:
                # There was no completion option in the data in memory
                # Get completion options from the API
                # self._api_reference
                if self._api_reference is not None:
                    uri = f"https://schemas.geodatahub.dk/{schema}.json"
                    current_options = self._api_reference.get_schema_options(uri, parameter)
                    if current_options is None:
                        msg = f"The following schema does not exist {uri} with parameter {parameter}"
                        LOG.error(msg)
                        # Close popup so users can read the error
                        self.popup().close()
                        return

                    # Store in memory so this is a lookup the next time
                    self._data[schema][parameter]['completion_options'] = current_options
                else:
                    msg = f"Completion options not available for {schema} with {parameter}"
                    LOG.error(msg)
                    # Close popup so users can read the error
                    self.popup().close()
                    return

            query_string = value if value else ""
            LOG.debug(f"Value search with string: {query_string}")
            completion_options = fuzzyfinder(query_string, current_options)
        else:
            # The users has not typed a logical operator for the latest query
            # or has started typing something that has not produced a full match yet.
            # In this case complete on schema and parameter
            if not user_is_typing_new_query:
                query_string = "".join([p for p in [schema, parameter] if p])  # Only use values that are not None
                if len(query_string) == 0:
                    query_string = text

            LOG.debug(f"Schema/parameter search with query: {query_string}")
            completion_options = fuzzyfinder(query_string, self.default_model)

        # Send the updated input query and completion options to the completer
        # Since the fuzzyfinder algorithm already has made the matching and ordered the results
        # an empty string is parsed to the completer algorithm so all completion options are shown.
        self.update_options(completion_options, '')


class APIStyledItemDelegate(QtWidgets.QStyledItemDelegate):
    """Custom styling for completion options from the API
    """

    def __init__(self, schema_model=None):
        """Create a new APIStyledItemDelegate

        Parameters
        ----------
        model: dict
            The possible schema completions ex. from the schemas.geodatahub.dk API.
        """
        QtWidgets.QStyledItemDelegate.__init__(self)
        self._schema_model = schema_model

    def paint(self, painter, option, index):
        """Create a custom styling

        This custom style makes the GDH search line response look better than the default Qt version.
        The base style is a single list without any formatting. This function extract information
        about the current completion options and formats then as

            KEY
            Description
            Schema

        Each item is separated such that the key is much larger than the rest of the text.
        """

        left_padding = 8

        try:
            try:
                # A float formatted as a string will contain a `.`. First check if a value conversion is
                # possible
                float(index.data(QtCore.Qt.DisplayRole))
            except ValueError:
                # If this was not a number type to split it on schema.parameter
                level0, level1 = index.data(QtCore.Qt.DisplayRole).split('.')
            else:
                # If this was a number just display a new list
                QtWidgets.QStyledItemDelegate.paint(self, painter, option, index)
                return
        except ValueError:
            # The string could not be split so just display all options as a simple list
            QtWidgets.QStyledItemDelegate.paint(self, painter, option, index)
        else:

            painter.save()
            adjusted_option = option

            key_rect = QtCore.QRect(option.rect.left() + left_padding,
                                    option.rect.top(),
                                    option.rect.width(),
                                    option.rect.height() / 2)
            desc_rect = QtCore.QRect(option.rect.left() + left_padding,
                                     key_rect.bottom(),
                                     option.rect.width(),
                                     option.rect.height() / 4)

            schema_rect = QtCore.QRect(option.rect.left() + left_padding,
                                       desc_rect.bottom(),
                                       option.rect.width(),
                                       option.rect.height() / 4)

            painter.restore()

            adjusted_option.backgroundBrush = QtGui.QBrush(QtCore.Qt.NoBrush)
            serifFont = QtGui.QFont("Helvetica", 14, QtGui.QFont.Light)
            painter.setFont(serifFont)
            painter.drawText(key_rect, QtCore.Qt.AlignVCenter, level1)

            serifFont = QtGui.QFont("Times", 10)
            painter.setFont(serifFont)
            description = "No description exists for this schema"
            if self._schema_model:
                try:
                    description = self._schema_model[level0][level1]["description"]
                except KeyError:
                    # The schema did not have a description parameter
                    pass

            painter.drawText(desc_rect, QtCore.Qt.AlignLeft, "Description: {}".format(description))
            painter.drawText(schema_rect, QtCore.Qt.AlignLeft, "schema: {}".format(level0))

    def sizeHint(self, option, index):
        """Set the size (height/width) of items in the completer view
        """
        s = QtCore.QSize(75, 20)
        s.setHeight(75)
        return s
