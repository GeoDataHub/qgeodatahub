**This repository is part of a larger experiment to build a geoscientific data catalog using modern cloud architectures with a special focus on collecting all data types within a single system to break down silos.**

**The project is stable but not production-ready and no longer maintained in its current form. It's released to inspire others and showcase how to build such a platform. See [getgeodata](getgeodata.com) for more.**

**If you have technical questions about the design (not the installation or setup) you are welcome to contact hello@getgeodata.com**

# QGeoDataHub
**QGeoDataHub** is a QGIS plugin to interact with the GeoDataHub platform directly from QGIS.

![Screenshot of the QGIS plugin](https://docs.geodatahub.dk/assets/img/qgis-plugin-screenshot.png)

The plugin support,
* Search by parameter, datatype, organization or spatial location
* Access to all metadata attributes directly from QGIS
* Upload and download of metadata 
* Update metadata attributes

## Install & getting started
Add the public geodatahub repository to QGIS: http://plugins.geodatahub.dk/qgis/plugins-releases.xml

Please see the detailed [getting started](https://docs.geodatahub.dk/QGIS-plugin/getting-started/) guide for more.

## Contributions are welcome
You are always welcome to submit bugfixes or new features as merge requests. We are happy to review and guide external commits to the plugin.

## Bugs
If you find any bugs please send them to: support@geodatahub.dk
