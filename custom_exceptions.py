# Custom GDH exceptions
class Error(Exception):
    """Base class for other exceptions"""
    pass


class SearchSyntaxError(Error):
    """Raised when there are errors in the search line syntax"""
    pass
