# coding=utf-8
"""DockWidget test.

.. note:: This program is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation; either version 2 of the License, or
     (at your option) any later version.

"""

__author__ = 'support@geodatahub.dk'
__date__ = '2019-06-24'
__copyright__ = 'Copyright 2019, NextGen Geophysics'

import unittest
import time

#from PyQt5.QtGui import QDockWidget
from PyQt5.QtWidgets import QDockWidget

from qgis.core import QgsPointXY, QgsFeature, QgsFields, QgsField

from qGeoDataHub import qGeoDataHub
from qGeoDataHub_dockwidget import qGeoDataHubDockWidget
from geodatahub.models import Dataset
from search_autocomplete import add_autocomplete, text_to_parameters
from custom_exceptions import SearchSyntaxError

from test.utilities import get_qgis_app

from .qgis_interface import MockConfigManager, MockAuthManager
from .env import config
QGIS_APP, CANVAS, IFACE, PARENT = get_qgis_app()


class qGeoDataHubMainTest(unittest.TestCase):
    """Test dockwidget works."""

    # Dataset that simulates the structure of what the user might
    # input
    mock_user_input = [
        {"datatype": "foo",
         "description": "A test dataset",
         "larger_work": {},
         "distribution_info": {},
         "result": "c:/foo.png",
         "projected_geometry": {
             "type": "Point",
             "coordinates": [12, 56]
         }}
    ]
    test_user_input_data = [Dataset(**md) for md in mock_user_input]

    # Dataset that simulates the structure of what the backend would
    # return
    mock_backend_response = [
        {"identifier": 1,
         "datatype": "foo",
         "description": "A test dataset",
         "larger_work": {},
         "distribution_info": {},
         "result": "c:/foo.png",
         "projected_geometry": {
             "type": "Point",
             "coordinates": [12, 56]
         }},
        {
            "identifier": "b6e20198-d50e-4321-9c77-c82af7b54c04",
            "created_at": "0001-01-01T00:00:00Z",
            "updated_at": "0001-01-01T00:00:00Z",
            "datatype": "https://schema.geodatahub.dk/organizations/geus/geophysics/wenner.json",
            "citation": "",
            "description": "Wenner",
            "larger_work": "",
            "distribution_info": {
                "Dataset Id": 1,
                "Project Id": 1
            },
            "result": {
                "download_link": "http://gerda.geus.dk"
            },
            "projected_geometry": {
                "type": "LineString",
                "coordinates": [
                    [
                        10.319948,
                        55.562133
                    ]
                ]
            }
        }
    ]
    test_backend_response_data = [Dataset(**md) for md in mock_backend_response]

    def setUp(self):
        """Runs before each test."""
        self.qgdh = qGeoDataHub(IFACE)
        self.qgdh.run()  # Activate plugin
        self.qgdh.dockwidget = qGeoDataHubDockWidget()
        self.qgdh.cfg = MockConfigManager()
        self.qgdh.am = MockAuthManager()

    def tearDown(self):
        """Runs after each test."""
        self.dockwidget = None

    def test_init_ok(self):
        """Test we can init the main object."""
        self.assertEqual(IFACE, self.qgdh.iface)

    def test_login_default_label_text_without_refresh(self):
        """Test the login with default text in search field"""
        self.assertFalse(self.qgdh.login_to_gdh())

    def test_login_uuid_label_text_without_refresh(self):
        """Test the login with uuid formatted text in search field"""
        self.qgdh.dockwidget.searchLine.setText("9d82a53b-72cf-471f-9a66-2a5390018883")
        self.assertFalse(self.qgdh.login_to_gdh())

    def test_mock_login_without_refresh(self):
        """Test the login with a mock login function"""
        self.qgdh.dockwidget.searchLine.setText("45709117-1d56-489e-8887-b42823e04366")
        def MockLogin(token):
            return 'access_token', 'refresh_token', 'id_token'
        self.qgdh.api._auth.login = MockLogin
        self.assertTrue(self.qgdh.login_to_gdh())

    def test_search(self):
        """Test that filling in a search query returns a response"""
        self.qgdh.dockwidget.searchLine.setText("schema.parameter=value")
        res = self.qgdh.search_gdh()
        self.assertEqual(res, None)

    def test_datasets_to_features(self):
        """Test converting GDH datasets to native QGIS objects"""

        self.qgdh.gdh_layer = self.qgdh.create_layer()
        rest = self.qgdh.datasets_to_features(self.test_backend_response_data)
        assert "foo-point" in rest

        # Are the layers formatted correctly
        self.assertIsInstance(rest["foo-point"], list)
        self.assertIn("https://schema.geodatahub.dk/organizations/geus/geophysics/wenner.json-linestring", rest)

        dset = rest["foo-point"][0]
        self.assertEqual(dset.attribute("datatype"), "foo")
        self.assertEqual(dset.attribute("distribution_info"), {})
        self.assertEqual(dset.attribute("result"), "c:/foo.png")
        self.assertEqual(dset.attribute("identifier"), 1)

        dset = rest["https://schema.geodatahub.dk/organizations/geus/geophysics/wenner.json-linestring"][0]
        self.assertEqual(dset.attribute("datatype"), "https://schema.geodatahub.dk/organizations/geus/geophysics/wenner.json")
        self.assertEqual(dset.attribute("distribution_info"), self.test_backend_response_data[1].distribution_info)
        self.assertEqual(dset.attribute("identifier"), "b6e20198-d50e-4321-9c77-c82af7b54c04")
#        self.assertEqual(dset.geometry().asJson()["type"], self.test_backend_response_data[1].projected_geometry["type"])
#        self.assertDictContainsSubset(dset.geometry().asJson(), self.test_backend_response_data[1].projected_geometry)


    def test_features_to_datasets(self):
        """Test converting QGIS features to native GDH datasets"""
        self.qgdh.gdh_layer = self.qgdh.create_layer()
        rest = self.qgdh.datasets_to_features(self.test_user_input_data)
        self.qgdh.gdh_layer.dataProvider().addFeatures(rest["foo-point"])

        # Convert features back
        datasets = self.qgdh.features_to_datasets()
        self.assertEqual(datasets[0].datatype, self.test_user_input_data[0].datatype)
        # Add more tests

    @unittest.skip("Test fails but manual test of the feature works")
    def test_layer_changes_are_uploaded(self):
        """Test that adding/deleting/changing a feature gets uploaded"""

        # Used to mock the backend database
        mock_backend_data = []

        def mock_call_endpoint(*args, **kwargs):
            """Mock version of the method to upload new datasets
            """
            payload = kwargs["payload"]
            # Users cannot set these parameters
            del payload["identifier"]
            del payload["owner"]
            del payload["organization"]

            mock_backend_data.append(payload)
            return {"id": 1}

        self.qgdh.gdh_layer = self.qgdh.create_layer()
        self.qgdh.api._call_endpoint = mock_call_endpoint

        # Create some test features
        rest = self.qgdh.datasets_to_features(self.test_user_input_data)

        # Add test data to a editable layer
        self.qgdh.gdh_layer.startEditing()
        self.qgdh.gdh_layer.addFeatures(rest)
        self.qgdh.gdh_layer.commitChanges()

        print("1 {}".format(self.qgdh.gdh_layer.featureCount()))
        # Test that the data set intact through the mock API call
        self.assertEqual(len(mock_backend_data), len(self.test_user_input_data))
        for itt, md in enumerate(self.mock_user_input):
            self.assertEqual(md, mock_backend_data[itt])

        # Open layer and edit an attribute value
        self.qgdh.gdh_layer.startEditing()
        feature_ids = [f.id() for f in self.qgdh.gdh_layer.getFeatures()]
        self.qgdh.gdh_layer.changeAttributeValue(feature_ids[0], 0, "bar")
        self.qgdh.gdh_layer.commitChanges()

        print("2 {}".format(self.qgdh.gdh_layer.featureCount()))
        # Test that the data set intact through the mock API call
        self.assertEqual(len(mock_backend_data), len(self.test_user_input_data) + 1)

        # Open layer and delete a feature
        self.qgdh.gdh_layer.startEditing()
        feature_ids = [f.id() for f in self.qgdh.gdh_layer.getFeatures()]
        self.qgdh.gdh_layer.deleteFeature(feature_ids[0])
        self.qgdh.gdh_layer.commitChanges()

        print("3 {}".format(self.qgdh.gdh_layer.featureCount()))
        # Test that the data set intact through the mock API call
        self.assertEqual(len(mock_backend_data), len(self.test_user_input_data) + 2)

    def test_auto_completion_no_data_init(self):
        """Ensure the autocomplete logic without data works as expected

        Calling the completer without initial data will force a call to get
        schemas via the API.
        """
        search_line = self.qgdh.dockwidget.searchLine
        # Add a completer without any data
        _, completer = add_autocomplete(search_line)
        time.sleep(4)
        print(completer._data)
        self.assertIsNotNone(completer)
        self.assertEqual(completer.currentCompletion(), "")

    def test_auto_completion_data_init(self):
        """Ensure the autocomplete logic with data works as expected

        The completer logic is tested in a seperate class. This only tests
        that the completer init works.
        """
        data = {
            "level0_0": {
                "level1_0": {"completion_options": ["a", "b", "c"]},
                "level1_1": {"completion_options": ["one", "two", "three"]},
                "level1_2": {"completion_options": ["foo", "bar", "foobar"]}
            },
            "level0_1": {
                "level1_3": {"completion_options": ["d", "e", "f"]},
                "level1_4": {"completion_options": ["four", "five", "six"]},
                "level1_5": {"completion_options": ["oof", "rab", "roof"]},
                "level1_6": {"completion_options": ["oof space", "rab space", "roof"]}
            },
            "level0_2": {
                "level1 3": {"completion_options": ["d", "e", "f"]},
                "level1 4": {"completion_options": ["four", "five", "six"]},
            },
        }
        search_line = self.qgdh.dockwidget.searchLine

        # Add a completer without any data
        _, completer = add_autocomplete(search_line, data)
        self.assertIsNotNone(completer)
        self.assertEqual(completer.currentCompletion(), "level0_0.level1_0")

        # Test if the textline is empty
        completer.text_changed('')

        # Test the update function on the completer
        completer.text_changed('level0_1')
        self.assertEqual(completer.currentCompletion(), "level0_1.level1_3")
        completer.text_changed('level0_1.level1_5')
        self.assertEqual(completer.currentCompletion(), "level0_1.level1_5")
        completer.text_changed('level0_1.level1_5=')
        self.assertEqual(completer.currentCompletion(), "oof")
        completer.text_changed('level0_1.level1_5=s')
        self.assertEqual(completer.currentCompletion(), "")
        completer.text_changed('level0_1.level1_5=ra')
        self.assertEqual(completer.currentCompletion(), "rab")

        completer.text_changed('level0_1.level1_5=ra leve4')
        self.assertEqual(completer.currentCompletion(), "level0_1.level1_4")

        search_line.setText("level0_1.level1_5=ra leve4")  # Current text in line edit
        search_line.update_with_completion("level0_1.level1_4")  # Completion result
        self.assertEqual(search_line.text(), "level0_1.level1_5=ra level0_1.level1_4")  # Final string

        # Test completion with a space in the option
        search_line.setText("level0_1.level1_5=ra level0_1.level1_6=")  # Current text in line edit
        search_line.update_with_completion("oof space")  # Completion result
        self.assertEqual(search_line.text(), "level0_1.level1_5=ra level0_1.level1_6=\"oof space\"")  # Final string

        # Test completion with a space in level1
        search_line.setText("level0_2.level1")  # Current text in line edit
        search_line.update_with_completion("level0_2.level1 3")  # Completion result
        self.assertEqual(search_line.text(), "\"level0_2.level1 3\"")  # Final string

        search_line.setText("\"level0_2.level1 4\"=")  # Current text in line edit
        completer.text_changed('\"level0_2.level1 4\"=fiv')
        self.assertEqual(completer.currentCompletion(), "five")
        search_line.update_with_completion("five")  # Completion result
        self.assertEqual(search_line.text(), "\"level0_2.level1 4\"=five")  # Final string

        # Test completion with multiple logical operators
        search_line.setText("level0_1.level1_5=ra level0_1.level1_6>")  # Current text in line edit
        search_line.update_with_completion("oof space")  # Completion result
        self.assertEqual(search_line.text(), "level0_1.level1_5=ra level0_1.level1_6>\"oof space\"")  # Final string

        search_line.setText("level0_1.level1_5>ra level0_1.level1_6<")  # Current text in line edit
        search_line.update_with_completion("42")  # Completion result
        self.assertEqual(search_line.text(), "level0_1.level1_5>ra level0_1.level1_6<42")  # Final string

    def test_text_to_parameters(self):
        """Test the function that converts the searchline to parameters"""
        tests = [
            # INPUT                                                                  # OUTPUT                         # Number of matches
            ["",                                                                     (None, None, None, None),              0],
            ["missing/Periode",                                                      (None, None, None, None),              0],

            # Single parameter searches
            ["schema.key=value",                                                     ("schema", "key",   "=", "value"),     1],
            ["\"schema.key\"=value",                                                 ("schema", "key",   "=", "value"),     1],
            ["\"schema.key\"=42",                                                    ("schema", "key",   "=", "42"),        1],
            ["\"schema.key\"=\"value\"",                                             ("schema", "key",   "=", "value"),     1],
            ["\"schema.key 2\"=\"value\"",                                           ("schema", "key 2", "=", "value"),     1],
            ["\"schema.key 2\"=\"value 2\"",                                         ("schema", "key 2", "=", "value 2"),   1],
            ["\"schema 2.key 2\"=\"value 2\"",                                       ("schema 2", "key 2", "=", "value 2"), 1],
            ["schema 2.key 2=\"value 2\"",                                           ("schema 2", "key 2", "=", "value 2"), 1],
            ["schema 2.key 2  =  \"value 2\"",                                       ("schema 2", "key 2", "=", "value 2"), 1],
            ["schema 2.key 2  =  value 2",                                           ("schema 2", "key 2", "=", "value"),   1],

            # Other logical operators
            ["schema.key>value",                                                     ("schema", "key",   ">", "value"),     1],
            ["\"schema.key\">value",                                                 ("schema", "key",   ">", "value"),     1],
            ["\"schema.key\">42",                                                    ("schema", "key",   ">", "42"),        1],
            ["\"schema.key\">\"value\"",                                             ("schema", "key",   ">", "value"),     1],

            ["schema.key<value",                                                     ("schema", "key",   "<", "value"),     1],
            ["\"schema.key\"<value",                                                 ("schema", "key",   "<", "value"),     1],
            ["\"schema.key\"<42",                                                    ("schema", "key",   "<", "42"),        1],
            ["\"schema.key\"<\"value\"",                                             ("schema", "key",   "<", "value"),     1],

            # Multi-parameter searches
            ["schema.key=value schema.key2=value",                                   ("schema", "key2",  "=", "value"),     2],
            ["schema.key=value \"schema.key 3\"=value",                              ("schema", "key 3", "=", "value"),     2],
            ["schema.key=value \"schema.key 3\"=value \"schema.key 4\"=\"value 2\"", ("schema", "key 4", "=", "value 2"),   3],
            ["schema.key=value \"schema.key 3\"=value schema 2.key 4=\"value 2\"",   ("schema 2", "key 4", "=", "value 2"), 3],

            # Incomplete searches - Single parameter
            ["schema.key=",                                                          ("schema", "key", "=", None),          1],
            ["schema.key",                                                           ("schema", "key", None, None),         1],
            ["schema.",                                                              ("schema", None,  None, None),         1],
            ["\"schema.key 2\"",                                                     ("schema", "key 2", None, None),       1],
            ["schema.key 2",                                                         ("schema", "key 2", None, None),       1],

            # Incomplete searches - Multi parameter
            ["schema.key=value \"schema.key 3\"=",                                   ("schema", "key 3", "=", None),        2],
            ["schema.key=value \"schema.key 3\"",                                    ("schema", "key 3", None, None),       2],
            ["schema.key=value schema.key 3",                                        ("schema", "key 3", None, None),       2],
            ["schema.key=value schema.",                                             ("schema", None,    None, None),       2]
        ]
        for search_string, expected, num_matches in tests:
            actual = text_to_parameters(search_string)
            self.assertEqual(actual, expected)
            self.assertEqual(len([x for x in text_to_parameters(search_string, only_last_match=False) if x]), num_matches)

    def test_convert_searchline_to_json(self):
        """Test the function that converts the searchline to json"""
        tests = [
            # INPUT               # OUTPUT
            ["schema.key=value", {"https://schema.geodatahub.dk/schema.json": {"key": "value"}}],
            ["schema.key=value schema.key2=value", {"https://schema.geodatahub.dk/schema.json": {"key": "value", "key2": "value"}}],
            ["schema.key=value   schema.key2=value ", {"https://schema.geodatahub.dk/schema.json": {"key": "value", "key2": "value"}}],
            ["schema.key=value \t\n schema.key2=value", {"https://schema.geodatahub.dk/schema.json": {"key": "value", "key2": "value"}}],
            ["3d_survey.operator=\"Aker Exploration AS\"", {"https://schema.geodatahub.dk/3d_survey.json": {"operator": "Aker Exploration AS"}}],
            ["3d_survey.operator=\"Total E&P Norge AS\"", {"https://schema.geodatahub.dk/3d_survey.json": {"operator": "Total E&P Norge AS"}}],
            ["organizations/geus/hydro.purpose=\"vand\"", {"https://schema.geodatahub.dk/organizations/geus/hydro.json": {"purpose": "vand"}}],
            ["\"organizations/geus/hydro.purpose name\"=\"vand\"", {"https://schema.geodatahub.dk/organizations/geus/hydro.json": {"purpose name": "vand"}}],
            ["\"organizations/geus/hydro.water hardness\"=234", {"https://schema.geodatahub.dk/organizations/geus/hydro.json": {"water hardness": 234}}],
            ["\"organizations/geus/hydro.water hardness\"=\"23\"", {"https://schema.geodatahub.dk/organizations/geus/hydro.json": {"water hardness": 23}}],

            # Advanced operators
            ["schema.key>value schema.key2=value", {"https://schema.geodatahub.dk/schema.json": {"key": {"$gt": "value"}, "key2": "value"}}],
            ["schema.key<value schema.key2=value", {"https://schema.geodatahub.dk/schema.json": {"key": {"$lt": "value"}, "key2": "value"}}],
            ["schema.key<value schema.key2>value", {"https://schema.geodatahub.dk/schema.json": {"key": {"$lt": "value"}, "key2": {"$gt": "value"}}}],

            # Queries that should fail
            ["schema.key=value .key2=value", SearchSyntaxError],
            ["schema.key=value schema.key2", SearchSyntaxError],
            ["schema.key=value schema.key2=", SearchSyntaxError]
        ]
        for search_string, expected in tests:
            if isinstance(expected, dict):
                actual = self.qgdh.searchline_to_json(search_string)
                self.assertDictEqual(actual, expected)
            else:
                with self.assertRaises(SearchSyntaxError):
                    self.qgdh.searchline_to_json(search_string)

        # Test a valid query against the API
        # The schema does not exist but it should return a 200 and an empty list
        self.assertEqual(self.qgdh.api.searchDataset({"3d_survey": {"operator": "Total E&P Norge AS"}}), [])


if __name__ == "__main__":
    suite = unittest.makeSuite(qGeoDataHubDialogTest)
    runner = unittest.TextTestRunner(verbosity=2)
    runner.run(suite)

