# -*- coding: utf-8 -*-
"""
/***************************************************************************
 qGeoDataHubFilterWidget
                             -------------------
        begin                : 2019-06-24
        git sha              : $Format:%H$
        copyright            : (C) 2019 by NextGen Geophysics
        email                : support@geodatahub.dk
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""
import logging as lg
import sys
from itertools import chain
from collections import Counter

# Qt imports
from PyQt5.QtWidgets import (
    QApplication, QWidget, QHBoxLayout, QPushButton,
    QDockWidget, QLineEdit, QLabel, QGroupBox,
    QVBoxLayout, QCompleter, QScrollArea, QAbstractScrollArea,
    QSpacerItem, QSizePolicy, QFrame
)
from PyQt5.QtCore import pyqtSignal, pyqtSlot, Qt

# QGIS imports
from qgis.gui import (
    QgsMapToolEmitPoint,
    QgsRubberBand,
    QgsMapTool,
    QgsCheckableComboBox
)

from qgis.core import (
    QgsPointXY,
    QgsRectangle,
    QgsWkbTypes,
    QgsCoordinateReferenceSystem,
    QgsProject,
    QgsCoordinateTransform
)

from search_autocomplete import APISchemas

LOG = lg.getLogger("qgeodatahub")


class SchemaWidget():

    def __init__(self, schema_name = None, api = None):
        self.completers = {}
        self.full_schema_uri = None
        self.api = api

    def generate_ui_from_schema(self, schema, title=None):
        """Convert a dataset schema to a Qt Layout

        This function takes a dict with the paramaters relevant for a single
        dataset schema and returns a GUI (Qt layout) to help users explore the
        schema.

        Ex. the following dict,

          { "name": "string" }

        returns a GUI with a QEditLine and a label with the text "name".

        Parameters
        ----------
        schema (dict):
            JSON schema encoded parameters
        title (str):
            Name of the schema. Use as the group title.

        Returns
        --------
        UI (QGroupBox):
            A GUI ready for a QGIS panel
        """
        try:
            self.full_schema_uri = schema["$id"]
        except KeyError:
            raise KeyError("The schema is not valid. $id property is missing.")

        try:
            try:
                group = QGroupBox(schema["title"])
            except KeyError:
                group = QGroupBox()
            group.full_schema_uri = self.full_schema_uri  # Store to extract during search
            layout = QVBoxLayout()

            for itt, (name, param) in enumerate(schema["properties"].items()):
                if param["type"] == "number":
                    param_field = QHBoxLayout()
                    param_field_min = QLineEdit()
                    param_field_min.parameter = name
                    param_field_min.min = True
                    param_field_min.setPlaceholderText("min")
                    param_field_max = QLineEdit()
                    param_field_max.parameter = name
                    param_field_max.max = True
                    param_field_max.setPlaceholderText("max")

                    try:
                        param_field_min.setToolTip(param["description"])
                        param_field_max.setToolTip(param["description"])
                    except KeyError:
                        pass

                    param_field.addWidget(param_field_min)
                    param_field.addWidget(param_field_max)

                    label = QLabel(name)
                    label.setBuddy(param_field_min)
                    layout.addWidget(label)
                    try:
                        layout.addWidget(param_field)
                    except TypeError:
                        layout.addLayout(param_field)

                elif param["type"] == "string":
                    param_field = QLineEdit()
                    param_field.parameter = name

                    if "example" in param:
                        param_field.setPlaceholderText(param["example"])

                    try:
                        param_field.setToolTip(param["description"])
                    except KeyError:
                        pass

                    # Update completion options when the text changes
                    param_field.textChanged.connect(lambda x, schema=self.full_schema_uri, parameter=name, lineedit=param_field: self.check_completer(x, schema, parameter, lineedit))

                    label = QLabel(name)
                    label.setBuddy(param_field)
                    layout.addWidget(label)

                    try:
                        layout.addWidget(param_field)
                    except TypeError:
                        layout.addLayout(param_field)
                else:
                    # TODO: Make this a real error once more types are implemented
                    LOG.debug(f"FUTURE ERROR: Schema contains invalid type {param['type']}")

        except KeyError as e:
            LOG.error(f"Invalid schema file: {schema}. Missing key {str(e)}")
            return None

        layout.addStretch(1)
        group.setLayout(layout)

        return group

    def get_completion_options(self, schema_uri, parameter):
        try:
            # Get completion options from the data that is already available
            current_options = self.completers[parameter]['completion_options']
        except KeyError:
            # There was no completion option in the data in memory
            # Get completion options from the API
            # self._api_reference
            current_options = self.api.get_schema_options(schema_uri, parameter)
            if current_options is None:
                msg = f"The following schema does not exist {schema_uri} with parameter {parameter}"
                LOG.error(msg)
                return

            # Store in memory so this is a lookup the next time
            self.completers[parameter]['completion_options'] = current_options

        return current_options

    def check_completer(self, text, schema_uri, parameter, lineedit):
        """Check if the completer has the options ready
        """
        if parameter not in self.completers:
            self.completers[parameter] = {}

            completer = QCompleter(["Fetching options..."])
            completer.setCaseSensitivity(Qt.CaseInsensitive)
            completer.setFilterMode(Qt.MatchContains)
            self.completers[parameter]["completer"] = completer
            lineedit.setCompleter(completer)
            completer.complete()  # Show message that the data is being fetched

        if "completion_options" not in self.completers[parameter]:
            completer_options = self.get_completion_options(schema_uri, parameter)
            if not completer_options:
                completer_options = ["No options available"]
            self.completers[parameter]["completer"].model().setStringList(completer_options)


class qGeoDataHubFilterDock(QDockWidget):
    """Dock for the advanced filter widget
    """
    closingPlugin = pyqtSignal()

    def __init__(self, parent=None, interface=None, main_widget=None):
        """Constructor."""
        super(qGeoDataHubFilterDock, self).__init__(parent)
        self.widget = qGeoDataHubFilterWidget(parent, interface, main_widget)
        self.setWidget(self.widget)

    def closeEvent(self, event):
        self.closingPlugin.emit()
        event.accept()


class qGeoDataHubFilterWidget(QWidget):
    """Main widget for the advanced filter widget
    """

    def __init__(self, parent=None, interface=None, main_widget=None, schemas=None):
        """Create a new filter widget

        Parameters
        -----------
        parent:
            Parent dock or window
        interface: Instance of QGIS interface
            Interface from the main window class
        main_widget: Instance of the main GDH widget
            Instance of the qGeoDataHub class
        """
        super(qGeoDataHubFilterWidget, self).__init__(parent)
        self.main_widget = main_widget

        if schemas:
            schemas.schema_ready.connect(self.add_schema_completer)
            self.schemas = schemas
            self.schema, _ = schemas.get_data()
        else:
            # The user supplied no data so let's get it from the schemes database
            self.schemas = APISchemas()
            self.schemas.schema_ready.connect(self.add_schema_completer)
            self.schema = {}
            self.schema_mapping = {}

        try:
            self.iface = interface
            self.canvas = self.iface.mapCanvas()
        except AttributeError:
            pass
        self.initUI()

    def initUI(self):
        """Construct the GUI"""
        self.main_layout = QVBoxLayout(self)

        # Add geographic search
        self.printButton = QPushButton("Draw search area")
        self.search_button = QPushButton("Search")

        if self.iface is not None:
            self.rect_tool = RectangleMapTool(self.iface.mapCanvas())
            self.printButton.clicked.connect(self.draw_search_area)

        self.search_button.clicked.connect(self.search)

        buttonLayout = QHBoxLayout()
        buttonLayout.addWidget(self.printButton)
        self.main_layout.addLayout(buttonLayout)
        geo_divider = self.create_divider()
        self.main_layout.addWidget(geo_divider)

        # Add organization/datatype search
        self.org_search_line = QgsCheckableComboBox()
        self.org_search_line.setDefaultText("Select an organization or datatype to search")

        org_search_label = QLabel("Search organization or datatype")
        org_search_label.setBuddy(self.org_search_line)

        self.main_layout.addWidget(org_search_label)
        self.main_layout.addWidget(self.org_search_line)
        schema_divider = self.create_divider()
        self.main_layout.addWidget(schema_divider)

        # Add area to search by a specific schema
        self.schema_search_line = QLineEdit()
        self.schema_search_line.setPlaceholderText("Find a schema by typing an organization or datatype")

        schema_search_label = QLabel("Search by schema")
        schema_search_label.setBuddy(self.schema_search_line)

        self.main_layout.addWidget(schema_search_label)
        self.main_layout.addWidget(self.schema_search_line)

        # widget that contains all the search parameters
        self.search_widget = QScrollArea()
        self.search_widget.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.search_widget.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.search_widget.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.search_widget.setWidgetResizable(True)
        self.main_layout.addWidget(self.search_widget)

        # Add button in the bottom
        self.main_layout.addWidget(self.search_button)

        self.setWindowTitle('')
        self.setLayout(self.main_layout)
        self.show()

    def create_divider(self):
        """Create a divider line

        This class creates a simple horizontal line to separate GUI elements
        visually to the user.

        Returns
        -------
        divider: QFrame
            The divider
        """
        separatorLine = QFrame()
        separatorLine.setFrameShape(QFrame.HLine)
        separatorLine.setFrameShadow(QFrame.Raised)
        sizePolicy = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Preferred)
        sizePolicy.setHeightForWidth(separatorLine.sizePolicy().hasHeightForWidth())
        separatorLine.setSizePolicy(sizePolicy)
        separatorLine.setStyleSheet("font: 7pt;")
        separatorLine.setLineWidth(0)
        separatorLine.setMidLineWidth(40)
        return separatorLine

    @pyqtSlot(dict)
    def add_schema_completer(self, schema = None):
        # Add auto complete
        if not schema:
            schema, _ = self.schemas.get_data()
        self.schema = schema

        # Custom renames
        # TODO: Replace with better schemas
        completer_options = [key for key in schema]
        self.schema_mapping["USGS - Geology - Geolog"] = "organizations/usgs/geophysics/geolog"
        self.schema_mapping["NPD - Geophysics - 3D Seismic"] = "3d_survey"
        self.schema_mapping["GEUS - Geophysics - Wenner"] = "organizations/geus/geophysics/wenner"

        completer_options[completer_options.index("organizations/usgs/geophysics/geolog")] = "USGS - Geology - Geolog"
        completer_options[completer_options.index("3d_survey")] = "NPD - Geophysics - 3D Seismic"
        completer_options[completer_options.index("organizations/geus/geophysics/wenner")] = "GEUS - Geophysics - Wenner"

        completer = QCompleter(completer_options)
        completer.setCaseSensitivity(Qt.CaseInsensitive)
        completer.setFilterMode(Qt.MatchContains)
        completer.activated.connect(self.add_schema_search)
        self.schema_search_line.setCompleter(completer)

        # Transform the list [[a,b,c],[b,c]] into [a,b,c,b,c]
        # Then count similar elements and convert to a string: Geophysics [3]
        all_tags = list(chain.from_iterable([s["datatype"] for s in self.schema.values()]))

        # Show the unique number of schemas for each type
        # TODO: Reactivate but current it does not work with the search by schema
        unique_tag_count = Counter(all_tags)
        # unique_search_str = sorted([f"{tag} [{count}]" for tag, count in unique_tag_count.items()])
        unique_search_str = sorted([f"{tag}" for tag, count in unique_tag_count.items()])
        self.org_search_line.addItems(unique_search_str)

        # Create lookup table to go from datatype (Wenner) to schema (organization/geus/geophysics/wenner)
        self.datatype_to_schema_mapping = { datatype: [] for datatype in all_tags}
        for schema, datalist in self.schema.items():
            for dt in datalist["datatype"]:
                self.datatype_to_schema_mapping[dt].append(schema)

    def add_schema_search(self, schema_name = None):
        if self.search_widget:
            # Remove old widgets before adding a new
            self.search_widget.deleteLater()

        # Generate a group with all filter parameters for that schema
        schema_key = self.schema_mapping[schema_name]
        sw = SchemaWidget(schema_name, self.main_widget.api)
        try:
            filter_widget = sw.generate_ui_from_schema(self.schema[schema_key])
        except Exception as e:
            LOG.error(str(e))

        # Create a scrollable area to store the filter parameters
        self.search_widget = QScrollArea()
        self.search_widget.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.search_widget.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        self.search_widget.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.search_widget.setWidgetResizable(True)
        self.search_widget.setWidget(filter_widget)

        # Insert the scroll area as the second last index (i.e. between the organization/datatype
        # search area and the search button)
        self.main_layout.insertWidget(8, self.search_widget)

    def draw_search_area(self):
        """Allow users to draw a rectangle to search within"""
        self.canvas.setMapTool(self.rect_tool)

    def search(self):
        """Perform geographic search"""
        query = {}

        # Add geographic search
        if self.rect_tool is not None and hasattr(self.rect_tool, "search_area"):
            map_epsg = self.canvas.mapSettings().destinationCrs().authid()
            api_epsg = "EPSG:4326"
            search_geometry = self.rect_tool.search_area

            if map_epsg != api_epsg:
                LOG.info(f"Transforming search area from {map_epsg} to {api_epsg}")
                xform = QgsCoordinateTransform(QgsCoordinateReferenceSystem(map_epsg),
                                               QgsCoordinateReferenceSystem(api_epsg),
                                               QgsProject.instance().transformContext())
                trans_result = search_geometry.transform(xform)
                if trans_result > 0:
                    LOG.error(f"Unable to transform search area from {map_epsg} to {api_epsg}. Reason {trans_result}")

            query["geometry"] = {"$contains": search_geometry.asWkt()}

            self.rect_tool.reset()

        # Add schemas the user requested
        for datatype in self.org_search_line.checkedItems():
            for schema in self.datatype_to_schema_mapping[datatype]:
                uri = f"https://schemas.geodatahub.dk/{schema}.json"
                query[uri] = ""

        # Add any schema specific parameters to the search
        LOG.info(dir(self.search_widget))
        schema_widget = self.search_widget.widget()
        if schema_widget is not None:
            for param_field in schema_widget.children():
                if hasattr(param_field, "text") and hasattr(param_field, "parameter") and param_field.text() != "":
                    # The user has typed something in the area
                    if hasattr(param_field, 'min'):
                        query[schema_widget.full_schema_uri] = {param_field.parameter: {"$gt": float(param_field.text())}}
                    elif hasattr(param_field, 'max'):
                        query[schema_widget.full_schema_uri] = {param_field.parameter: {"$lt": float(param_field.text())}}
                    else:
                        query[schema_widget.full_schema_uri] = {param_field.parameter: param_field.text()}

        if len(query) <= 0:
            LOG.error("Please select at least once parameter before searching")
        else:
            self.main_widget.search_gdh(query)


class RectangleMapTool(QgsMapToolEmitPoint):
    """Tool to allow users to draw a rectangle on the canvas"""

    def __init__(self, canvas):
        self.canvas = canvas
        QgsMapToolEmitPoint.__init__(self, self.canvas)
        self.rubberBand = QgsRubberBand(self.canvas, True)
        self.rubberBand.setColor(Qt.red)
        self.rubberBand.setWidth(1)
        self.reset()

    def reset(self):
        self.startPoint = self.endPoint = None
        self.isEmittingPoint = False
        self.rubberBand.reset(True)

    def canvasPressEvent(self, e):
        """Set the start corner point of the area
        """
        self.startPoint = self.toMapCoordinates(e.pos())
        self.endPoint = self.startPoint
        self.isEmittingPoint = True
        self.showRect(self.startPoint, self.endPoint)

    def canvasReleaseEvent(self, e):
        """Set the end corner point of the area
        """
        self.isEmittingPoint = False
        r = self.rectangle()
        if r is not None:
            self.search_area = self.rubberBand.asGeometry()
            self.deactivate()
        else:
            LOG.error("Unable to get extent of rectangle")

    def canvasMoveEvent(self, e):
        """Update the rectangle drawn in the map window"""
        if not self.isEmittingPoint:
            return

        self.endPoint = self.toMapCoordinates(e.pos())
        self.showRect(self.startPoint, self.endPoint)

    def showRect(self, startPoint, endPoint):
        """Draw rectangle

        Parameters
        -----------
        startPoint:
            Initial point in map coordinates
        endPoint:
            Current selected point in map coordinates
        """
        self.rubberBand.reset(QgsWkbTypes.PolygonGeometry)
        if startPoint.x() == endPoint.x() or startPoint.y() == endPoint.y():
            return

        point1 = QgsPointXY(startPoint.x(), startPoint.y())
        point2 = QgsPointXY(startPoint.x(), endPoint.y())
        point3 = QgsPointXY(endPoint.x(), endPoint.y())
        point4 = QgsPointXY(endPoint.x(), startPoint.y())

        self.rubberBand.addPoint(point1, False)
        self.rubberBand.addPoint(point2, False)
        self.rubberBand.addPoint(point3, False)
        self.rubberBand.addPoint(point4, True)    # true to update canvas
        self.rubberBand.show()

    def rectangle(self):
        """Convert corner points to rectangle
        """
        if self.startPoint is None or self.endPoint is None:
            print("No start or end point")
            return None
        elif (self.startPoint.x() == self.endPoint.x() or \
              self.startPoint.y() == self.endPoint.y()):
            print("Rectangle starts and ends at the same point")
            return None

        return QgsRectangle(self.startPoint, self.endPoint)

    def deactivate(self):
        QgsMapTool.deactivate(self)
        self.deactivated.emit()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = qGeoDataHubFilterDock()
    ex.show()
    sys.exit(app.exec_())
